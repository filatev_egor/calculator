public class Dec implements IMathActions{

    public int plus(String a, String b){
        int first = Integer.parseInt(a);
        int second = Integer.parseInt(b);
        return first+second;
    }
    public int minus(String a, String b){
        int first = Integer.parseInt(a);
        int second = Integer.parseInt(b);
        return first-second;
    }
    public int multiply(String a, String b){
        int first = Integer.parseInt(a);
        int second = Integer.parseInt(b);
        return first*second;
    }
    public int divide(String a, String b) throws Exception {
        int first = Integer.parseInt(a);
        int second = Integer.parseInt(b);
        if (second==0){
            throw new Exception("Недопустимо делить на ноль");
        }
        return first/second;
    }
}
