import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        calculator();
    }
    public static void calculator(){
        IMathActions act = new Dec();
        int system = 10;
        Scanner sc = new Scanner(System.in);
        sc.useDelimiter("\\n");
        boolean doing = true;
        do{
            try{
                System.out.println("Выберите системы счисления");
                boolean menu = true;
                while (menu){
                    System.out.println("1. Двоичная (Bin)");
                    System.out.println("2. Восьмеричная (Oct)");
                    System.out.println("3. Десятичная (Dec)");
                    System.out.println("4. Шестнадцатеричная (Hex)");
                    System.out.println("0. Выход");
                    String choose = sc.next();
                    switch (choose) {
                        case ("1"):
                            act = new Bin();
                            system = 2;
                            menu = false;
                            break;
                        case ("2"):
                            act = new Oct();
                            system = 8;
                            menu = false;
                            break;
                        case ("3"):
                            act = new Dec();
                            system = 10;
                            menu = false;
                            break;
                        case ("4"):
                            act = new Hex();
                            system = 16;
                            menu = false;
                            break;
                        case ("0"):
                            doing = false;
                            menu = false;
                            break;
                        default:
                            System.out.println("Выбран неправильный пункт меню. Попробуйте снова");
                            break;
                    }
                }
                if (doing) {
                    boolean tryFirst = true, trySecond = true;
                    int first, second;
                    String a="", b="";
                    System.out.println("Введите первое число");
                    while (tryFirst) {
                        try {
                            a = sc.next();
                            first = Integer.parseInt(a,system);
                            tryFirst = false;
                            if (a.length()>1 && a.charAt(0)=='0' && system!=2){
                                System.out.println("Число не двоичной системы не может начинаться с нуля. Повторите попытку.");
                                tryFirst = true;
                            }
                        }
                        catch (Exception e){
                            System.out.println("Было введено не число нужной системы счисления или слишком большое число по модулю. Повторите попытку.");
                        }
                    }
                    System.out.println("Введите второе число");
                    while (trySecond) {
                        try {
                            b = sc.next();
                            second = Integer.parseInt(b,system);
                            trySecond = false;
                            if (b.length()>1 && b.charAt(0)=='0' && system!=2){
                                System.out.println("Число не двоичной системы не может начинаться с нуля. Повторите попытку.");
                                trySecond = true;
                            }
                        }
                        catch (Exception e){
                            System.out.println("Было введено не число нужной системы счисления или слишком большое число по модулю. Повторите попытку.");
                        }
                    }
                    System.out.println("Введите знак действия: +, -, * или /");
                    boolean check = true;
                    while (check){
                        String symbol = sc.next();
                        int ans;
                        switch (symbol){
                            case ("+"):
                                ans = act.plus(a,b);
                                System.out.print("BIN: ");
                                System.out.println(Integer.toBinaryString(ans));
                                System.out.print("OCT: ");
                                System.out.println(Integer.toOctalString(ans));
                                System.out.print("DEC: ");
                                System.out.println(ans);
                                System.out.print("HEX: ");
                                System.out.println(Integer.toHexString(ans));
                                check=false;
                                break;
                            case ("-"):
                                ans = act.minus(a,b);
                                System.out.print("BIN: ");
                                System.out.println(Integer.toBinaryString(ans));
                                System.out.print("OCT: ");
                                System.out.println(Integer.toOctalString(ans));
                                System.out.print("DEC: ");
                                System.out.println(ans);
                                System.out.print("HEX: ");
                                System.out.println(Integer.toHexString(ans));
                                check=false;
                                break;
                            case ("*"):
                                ans = act.multiply(a,b);
                                System.out.print("BIN: ");
                                System.out.println(Integer.toBinaryString(ans));
                                System.out.print("OCT: ");
                                System.out.println(Integer.toOctalString(ans));
                                System.out.print("DEC: ");
                                System.out.println(ans);
                                System.out.print("HEX: ");
                                System.out.println(Integer.toHexString(ans));
                                check=false;
                                break;
                            case ("/"):
                                ans = act.divide(a,b);
                                System.out.print("BIN: ");
                                System.out.println(Integer.toBinaryString(ans));
                                System.out.print("OCT: ");
                                System.out.println(Integer.toOctalString(ans));
                                System.out.print("DEC: ");
                                System.out.println(ans);
                                System.out.print("HEX: ");
                                System.out.println(Integer.toHexString(ans));
                                check=false;
                                break;
                            default:
                                System.out.println("Был введён неправильный знак. Повторите попытку.");
                                break;
                        }
                    }
                }

            }
            catch (Throwable t){
                System.out.println(t.getMessage());
            }
        } while (doing);
    }
}