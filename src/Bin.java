public class Bin implements IMathActions{
    public int plus(String a, String b){
        int first = Integer.parseInt(a, 2);
        int second = Integer.parseInt(b, 2);
        return first+second;
    }
    public int minus(String a, String b){
        int first = Integer.parseInt(a, 2);
        int second = Integer.parseInt(b, 2);
        return first-second;
    }
    public int multiply(String a, String b){
        int first = Integer.parseInt(a, 2);
        int second = Integer.parseInt(b, 2);
        return first*second;
    }
    public int divide(String a, String b) throws Exception{
        int first = Integer.parseInt(a, 2);
        int second = Integer.parseInt(b, 2);
        if (second==0){
            throw new Exception("Недопустимо делить на ноль");
        }
        return first/second;
    }
}
