public interface IMathActions {

    public int plus(String a, String b);
    public int minus(String a, String b);
    public int multiply(String a, String b);
    public int divide(String a, String b) throws Exception;
}
